import { readFileSync, writeFileSync } from "fs";
import { join } from "path";

export const getMyFw = () => {
  const myFw = readFileSync(
    join(process.cwd(), "./api/storage/myPocketFirmware.json"),
    "utf-8"
  );
  return JSON.parse(myFw).myCurrentVersion;
};

export const setMyFw = (ver: string) => {
  try {
    const newVer = { myCurrentVersion: ver };
    writeFileSync(
      join(process.cwd(), "./api/storage/myPocketFirmware.json"),
      JSON.stringify(newVer)
    );
    return { error: false, ver, message: "Your firmware version was updated!" };
  } catch (err) {
    return {
      error: true,
      message: "Error saving your pocket firmware version.",
      code: err,
    };
  }
};
