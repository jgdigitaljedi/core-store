import fetch from "node-fetch";

const checkAppUpdate = () => {
  return new Promise((resolve, reject) => {
    return fetch(
      "https://gitlab.com/jgdigitaljedi/core-store/-/raw/main/package.json",
      {
        method: "get",
        headers: { Accept: "application/json" },
      }
    )
      .then((res: any) => {
        resolve(res.json());
      })
      .catch((err) => {
        reject({
          error: true,
          message: "Error checking for app update.",
          code: err,
        });
      });
  });
};

export default checkAppUpdate;
