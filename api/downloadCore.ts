import { pipeline } from "node:stream";
import { promisify } from "node:util";
import fetch from "node-fetch";
import { createWriteStream } from "node:fs";
import { join } from "node:path";

export async function downloadCore(core, errorMessage) {
  const streamPipeline = promisify(pipeline);
  const response = await fetch(core.download);
  if (!response.ok) {
    // console.log(chalk.red(errorMessage));
    return Promise.resolve();
  }
  await streamPipeline(
    response.body,
    createWriteStream(join(__dirname, `../newCores/${core.platform}.zip`))
  );
}
