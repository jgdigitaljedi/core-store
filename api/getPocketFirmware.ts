import fetch from "node-fetch";
import * as cheerio from "cheerio";

export async function getPocketFirmwareVersion() {
  try {
    const pocketPage = await fetch(
      "https://www.analogue.co/support/pocket/firmware"
    );
    const body = await pocketPage.text();
    const $ = cheerio.load(body);
    const className = $("[class*=firmwares_version__]").attr("class");
    const container = $(`.${className} > p`);
    const filtered = Array.from(container)[0];
    const latest = $(filtered).text();
    return latest;
  } catch (err) {
    return {
      error: true,
      message: "Error getting current firmware version",
      code: err,
    };
  }
}
