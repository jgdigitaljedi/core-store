import fetch from "node-fetch";

const getMasterRepoList = () => {
  return new Promise((resolve, reject) => {
    return fetch(
      "https://gitlab.com/jgdigitaljedi/pocket-update-notifier/-/raw/main/extraScripts/getAllCoreInfo/allCoresInfo.json",
      {
        method: "get",
        headers: { Accept: "application/json" },
      }
    )
      .then((res: any) => {
        resolve(res.json());
      })
      .catch((err) => {
        reject({
          error: true,
          message: "Error fetching cores list.",
          code: err,
        });
      });
  });
};

export default getMasterRepoList;
