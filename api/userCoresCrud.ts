import { readFileSync, writeFileSync } from "fs";
import { join } from "path";
import { IUserCore } from "../src/util/coreData";
import { ICrudError } from "../src/util/readWrite";

export const getUserCores = (): IUserCore[] | ICrudError => {
  try {
    const userCores = readFileSync(
      join(process.cwd(), "./api/storage/myCores.json"),
      "utf-8"
    );
    return JSON.parse(userCores);
  } catch (error) {
    return {
      error: true,
      message: "Failed to get your cores from storage.",
      code: error,
    };
  }
};

export const setUserCores = (newMyCores: IUserCore[]) => {
  try {
    writeFileSync(
      join(process.cwd(), "./api/storage/myCores.json"),
      JSON.stringify(newMyCores)
    );
    return newMyCores;
  } catch (error) {
    return { error: true, message: "Error saving your cores.", code: error };
  }
};
