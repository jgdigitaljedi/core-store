import { readFileSync, writeFileSync } from "fs";
import { join } from "path";
import { ICrudError } from "../src/util/readWrite";
import { IUserSettings } from "../src/util/userSettings";

export const getSettings = (): IUserSettings | ICrudError => {
  try {
    const settings = readFileSync(
      join(process.cwd(), "./api/storage/userSettings.json"),
      "utf-8"
    );
    return JSON.parse(settings);
  } catch (err) {
    return { error: true, message: "Error fetching user settings.", code: err };
  }
};

export const updateSettings = (
  newSettings: any
): IUserSettings | ICrudError => {
  try {
    const settings = getSettings();
    if ((settings as ICrudError).error) {
      return {
        error: true,
        message:
          "Error writing user settings: could fetch previous user settings.",
        code: (settings as ICrudError).error,
      };
    }
    const updated = { ...settings, ...newSettings };
    writeFileSync(
      join(process.cwd(), "./api/storage/userSettings.json"),
      JSON.stringify(updated, null, 2)
    );
    return updated;
  } catch (err) {
    return { error: true, message: "Error updating user settings.", code: err };
  }
};
