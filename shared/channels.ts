interface Channels {
  [key: string]: string;
}

const channels: Channels = {
  USER_SETTINGS: "getUserSettings",
  SET_USER_SETTINGS: "setUserSettings",
  USER_CORES: "getUserCores",
  SET_USER_CORES: "setUserCores",
  MASTER_REPO_LIST: "getMasterRepoList",
  ANALOGUE_FW: "checkPocketFirmware",
  MY_FW: "getMyFw",
  SET_MY_FW: "setMyFw",
  APP_UPDATE: "checkAppUpdate",
};

export default channels;
