# Publishing guide

This is really just a note for myself to remind myself the process in which I cut releases.

1. run `npm run build`
2. run `git add -A`
3. run `git commit -m "<commit message>"`
   - look at `.versionrc.json` for commit prefixes to make sure your commit is prefixed properly
4. run `npm run release:<which>` **major, minor, patch**
   - This should increment version and generate changelog
5. run `npm publish`
   - This SHOULD run tests and lint, create the tag, publish to npm, and push tag and master to Gitlab
