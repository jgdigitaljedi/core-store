# Core Store

This is a cross-platform desktop app meant to serve as a companion to your Analogue Pocket. There are versions of this app available for MacOS, Windows, and Linux.

![Core Store screenshot](/coreStoreScreenshot.png "Core Store screenshot")

What you can do with this app:

- check for Analogue Pocket firmware updates
- download Analogue Pocket openFPGA cores
- get notified if a new version of an openFPGA core you are using has an update and download the new version

## Disclaimer

I'm abandoning this project and it will not build in its current state and there are already some excellent tools out there for updating your Analogue Pocket. This was a good learning experience, and now that I am aware of Tauri, I would never go down the electron path again. There is already a great cross-platform frontend for Analogue Pocket core downloads an updates you can download at [Pocket Sync](https://github.com/neil-morrison44/pocket-sync).
