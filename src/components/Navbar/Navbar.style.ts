import { ThemeUIStyleObject } from "theme-ui";

export const navContainerStyle: ThemeUIStyleObject = {
  width: "100%",
  mt: 2,
  mb: 4,
  justifyContent: "space-evenly",
  flexDirection: ["column", "row"],
};

export const navlinkLinkStyle = (isActive?: boolean): ThemeUIStyleObject => {
  return {
    a: {
      color: isActive ? "primary" : "text",
      fontWeight: isActive ? "bold" : "normal",
      fontSize: "1.5rem",
      mx: 2,
      textDecoration: "none",
    },
  };
};
