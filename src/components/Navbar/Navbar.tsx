import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Flex } from "theme-ui";
import { navContainerStyle } from "./Navbar.style";
import {
  IoGameControllerSharp,
  IoSettingsSharp,
  IoInformationCircleSharp,
} from "react-icons/io5";

const Navbar: React.FC = () => {
  const navigate = useNavigate();
  const [currentRoute, setCurrentRoute] = useState(
    window.sessionStorage.getItem("path") || "/"
  );
  const onRouteChange = (path: string): void => {
    window.sessionStorage.setItem("path", path);
    setCurrentRoute(path);
    navigate(path);
  };

  return (
    <Flex sx={navContainerStyle}>
      <Button
        variant={currentRoute === "/" ? "navCurrent" : "nav"}
        onClick={() => onRouteChange("/")}
      >
        <IoGameControllerSharp alt-text="game controller" />
        Cores
      </Button>
      <Button
        variant={currentRoute === "/settings" ? "navCurrent" : "nav"}
        onClick={() => onRouteChange("/settings")}
      >
        <IoSettingsSharp alt-text="settings gear" />
        Settings
      </Button>
      <Button
        variant={currentRoute === "/about" ? "navCurrent" : "nav"}
        onClick={() => onRouteChange("/about")}
      >
        <IoInformationCircleSharp alt-text="information" />
        About
      </Button>
    </Flex>
  );
};

export default Navbar;
