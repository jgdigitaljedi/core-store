import { useNotifications } from "@/util/notificationContext";
import React from "react";
import { Alert, Box, Close, Text } from "theme-ui";

const Notify: React.FC = () => {
  const { notifications, clearNotification } = useNotifications();

  const dismissTimeout = (id: number, timeout: number) => {
    setTimeout(() => clearNotification(id), timeout);
  };

  return (
    <Box
      sx={{
        bottom: "1rem",
        right: "1rem",
        zIndex: 10,
        position: "fixed",
        boxShadow: "card",
      }}
    >
      {notifications.map(({ id, message, variant, timeout }) => {
        if (timeout && timeout > 0) {
          dismissTimeout(id, timeout);
        }

        return (
          <Alert key={id} variant={variant} sx={{ mt: 2 }}>
            <Text>{message}</Text>
            <Close
              onClick={() => clearNotification(id)}
              sx={{ ml: 3, cursor: "pointer" }}
            />
          </Alert>
        );
      })}
    </Box>
  );
};

export default Notify;
