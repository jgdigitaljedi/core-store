import { ICoreRepo, IUserCore } from "@/util/coreData";
import React, { useMemo } from "react";
import { Box, Button, Text } from "theme-ui";
import sortBy from "lodash/sortBy";
import { highlightCell } from "./CoresTable.style";

interface CoresTableProps {
  cores?: ICoreRepo[] | null;
  userCores?: IUserCore[];
}

const CoresTable: React.FC<CoresTableProps> = ({ cores, userCores }) => {
  const combinedCores = useMemo(() => {
    if (cores) {
      return sortBy(
        cores.map((core: ICoreRepo) => {
          const userCore = userCores?.filter(
            (uc: IUserCore) => uc.id === core.id
          );
          return {
            ...core,
            myVersion: userCore?.length ? userCore[0].myVersion : "",
          };
        }),
        [(o) => !o.myVersion, (o) => o.myVersion === o.release, "platform"]
      );
    } else {
      return [];
    }
  }, [cores, userCores]);

  const downloadFile = (core: ICoreRepo) => {
    const dlSplit = core.download.split("/");
    const fileName = dlSplit[dlSplit.length - 1];
    const anchor = document.createElement("a");
    anchor.href = core.download;
    anchor.download = fileName;
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  };

  return (
    <Box sx={{ width: "100%", p: 2 }}>
      <Box
        as="table"
        sx={{
          width: "100%",
          borderCollapse: "collapse",
          borderColor: "black",
          borderWidth: "1px",
          borderStyle: "solid",
          "td, th": {
            p: 2,
            borderColor: "black",
            borderWidth: "1px",
            borderStyle: "solid",
          },
        }}
      >
        <Box as="thead" sx={{ backgroundColor: "bgLighter" }}>
          <Box as="tr">
            <Box as="th">Platform</Box>
            <Box as="th">Developer</Box>
            <Box as="th">Your version</Box>
            <Box as="th">Current version</Box>
            <Box as="th">Action</Box>
          </Box>
        </Box>
        <Box as="tbody">
          {combinedCores.map((core: any, index: number) => {
            const upToDate = core.myVersion.trim() === core.release.trim();
            const dlOrUd = core.myVersion ? "Update" : "Download";
            return (
              <Box
                as="tr"
                sx={{
                  backgroundColor:
                    index % 2 === 0 ? "transparent" : "bgLighter",
                }}
                key={core.id}
              >
                <Box as="td">{core.platform}</Box>
                <Box as="td">{core.developer}</Box>
                <Box as="td">
                  <Text sx={highlightCell(upToDate, !!core.myVersion)}>
                    {core.myVersion}
                  </Text>
                </Box>
                <Box as="td">
                  <Text sx={highlightCell(upToDate, !!core.myVersion)}>
                    {core.release}
                  </Text>
                </Box>
                <Box as="td">
                  <Button
                    disabled={upToDate}
                    onClick={() => downloadFile(core)}
                    variant={
                      upToDate
                        ? "disabled"
                        : core.myVersion
                        ? "secondary"
                        : "primary"
                    }
                    aria-label={`${dlOrUd} ${core.platform} core`}
                  >
                    {dlOrUd}
                  </Button>
                </Box>
              </Box>
            );
          })}
        </Box>
      </Box>
    </Box>
  );
};

export default CoresTable;
