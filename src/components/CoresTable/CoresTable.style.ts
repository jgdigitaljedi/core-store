import { ThemeUIStyleObject } from "theme-ui";

export const coresTableHeaderStyle: ThemeUIStyleObject = {
  color: "primary",
  fontWeight: "bold",
};

export const highlightCell = (
  upToDate: boolean,
  hasCore: boolean
): ThemeUIStyleObject => {
  const highlight = !upToDate && hasCore;
  return {
    backgroundColor: highlight ? "highlight" : "transparent",
    color: highlight ? "black" : "text",
    p: highlight ? 1 : 0,
  };
};
