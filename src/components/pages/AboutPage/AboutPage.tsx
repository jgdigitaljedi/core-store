import React from "react";
import { Box, Flex, Heading } from "theme-ui";
import { AboutApp } from "./AboutApp";
import { SetupCard } from "./SetupCard";
import { UsageCard } from "./UsageCard";

const AboutPage: React.FC = () => {
  return (
    <Flex
      sx={{
        flexDirection: "column",
        width: "100%",
        alignItems: "center",
        height: "100%",
        pt: 3,
        justifyContent: "center",
      }}
    >
      <Heading as="h1">Core Store</Heading>
      <Flex sx={{ justifyContent: "center", pt: 3 }}>
        <AboutApp />
        <Box>
          <SetupCard />
          <UsageCard />
        </Box>
      </Flex>
    </Flex>
  );
};

export default AboutPage;
