import React from "react";
import { Box, Card, Divider, Flex, Heading, Link, Text } from "theme-ui";
import packageJson from "../../../../package.json";
import { aboutSubSectionStyle, instructionsCardStyle } from "./About.styles";

export const AboutApp: React.FC = () => {
  const openInBrowser = (url: string): void => {
    require("electron").shell.openExternal(url);
  };

  return (
    <Card
      sx={{
        ...instructionsCardStyle,
        mr: 3,
        minWidth: "13rem",
      }}
    >
      <Flex sx={aboutSubSectionStyle}>
        <Heading as="h3">About</Heading>
        <Text>version: {packageJson.version}</Text>
        <Box>
          <Text>website: </Text>
          <Link
            sx={{ cursor: "pointer" }}
            onClick={() =>
              openInBrowser("https://gitlab.com/jgdigitaljedi/core-store")
            }
          >
            GitLab
          </Link>
        </Box>
        <Text>author: Joey Gauthier</Text>
      </Flex>
      <Divider sx={{ width: "100%" }} />
      <Flex sx={aboutSubSectionStyle}>
        <Heading as="h3">Useful Links</Heading>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() => {
            openInBrowser("https://www.analogue.co/support/pocket/firmware");
          }}
          title="Analogue's Pocket firmware download page"
        >
          Pocket firmware
        </Link>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() =>
            openInBrowser("https://github.com/RetroDriven/Pocket_Updater")
          }
          title="Core updater for Windows"
        >
          Updater (RetroDriven)
        </Link>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() =>
            openInBrowser(
              "https://github.com/mattpannella/pocket_core_autoupdate_net"
            )
          }
          title="Core updater for Windows, Mac, and Linux"
        >
          Updater (mattpannella)
        </Link>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() =>
            openInBrowser(
              "https://gitlab.com/jgdigitaljedi/pocket-update-notifier"
            )
          }
          title="Core and firmware updater for Node.js"
        >
          Updater (me)
        </Link>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() =>
            openInBrowser(
              "https://joshcampbell191.github.io/openfpga-cores-inventory/analogue-pocket"
            )
          }
          title="List of cores with links"
        >
          Cores library site
        </Link>
      </Flex>
      <Divider sx={{ width: "100%" }} />
      <Flex sx={aboutSubSectionStyle}>
        <Heading as="h3">Built using:</Heading>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() => openInBrowser("https://www.electronjs.org/")}
          title="Electron.js website"
        >
          Electron
        </Link>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() => openInBrowser("https://reactjs.org/")}
          title="React.js website"
        >
          React
        </Link>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() => openInBrowser("https://vitejs.dev/")}
          title="Vite.js website"
        >
          Vite
        </Link>
        <Link
          sx={{ cursor: "pointer" }}
          onClick={() => openInBrowser("https://theme-ui.com/")}
          title="Theme-UI website"
        >
          Theme-UI
        </Link>
      </Flex>
    </Card>
  );
};
