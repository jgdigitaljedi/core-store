import { ThemeUIStyleObject } from "theme-ui";

export const instructionsCardStyle: ThemeUIStyleObject = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "space-between",
};

export const aboutSubSectionStyle: ThemeUIStyleObject = {
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
};
