import React from "react";
import { Box, Card, Heading } from "theme-ui";
import { instructionsCardStyle } from "./About.styles";

export const UsageCard: React.FC = () => {
  return (
    <Card sx={instructionsCardStyle}>
      <Heading sx={{ mb: 2 }} as="h2">
        Usage ("Cores" view)
      </Heading>
      <Box as="ol">
        <Box as="li">
          In the top of the cores view there is a thin row for firmware. If the
          current firmware version doesn't match your entered firmware version,
          you can use the button that will appear on the right side of this row
          to open the Analogue support page in your default browser and download
          the latest firmware. Once you update your firmware, make sure to
          update the version you have in the "Settings" view so the app can let
          you know when another firmware update is available.
        </Box>
        <Box as="li">
          In the cores table, each known core will have a row. Any core you have
          installed that has a newer version available will have an orange
          "Update" button and be at the top of the table. Clicking the "Update"
          button opens a download dialog so you can directly download the newest
          version of that core.
        </Box>
        <Box as="li">
          Any cores you have entered your version for that do not have updates
          will be listed next in the table.
        </Box>
        <Box as="li">
          The bottom section of the table will contain cores for which you have
          not entered a version. If you want to download one of these cores,
          there will be a "Download" button in the "Actions" column to do so.
          Just make sure that when you install new cores, you put the version of
          that core you downloaded into the "Settings" view so the app can check
          for updates for that core!
        </Box>
      </Box>
    </Card>
  );
};
