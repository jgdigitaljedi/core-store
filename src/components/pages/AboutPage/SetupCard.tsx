import React from "react";
import { Box, Card, Divider, Heading } from "theme-ui";
import { instructionsCardStyle } from "./About.styles";

export const SetupCard: React.FC = () => {
  return (
    <Card sx={instructionsCardStyle}>
      <Heading sx={{ mb: 2 }} as="h2">
        Set up ("Settings" view)
      </Heading>
      <Box as="ol">
        <Box as="li">
          Go to the "Settings" tab and select whichever options you wish to
          select in the top section.If you enable the checking for firmware
          updates, you'll need to add your firmware version in the corresponding
          field and it will need to be formatted exactly as it is on the
          Analogue support page (example: 1.1-beta-4).
        </Box>
        <Box as="li">
          In the cores section below, fill in the version of each core you are
          currently using. For any core you are not using, just leave the field
          for that core blank. Note that there are buttons beside each core
          field with the most recent version of that core on them. If you click
          any of these buttons, it will automatically fill that version into the
          corresponding field for you.
        </Box>
      </Box>
    </Card>
  );
};
