import { ThemeUIStyleObject } from "theme-ui";

export const thinRowStyle: ThemeUIStyleObject = {
  width: "100%",
  p: 2,
  justifyContent: ["center", "center", "space-between"],
  alignItems: "center",
  backgroundColor: "bgContrast",
  color: "primary",
  flexDirection: ["column", "column", "row"],
};

export const dataPointStyle: ThemeUIStyleObject = {
  color: "text",
  ml: 2,
};
