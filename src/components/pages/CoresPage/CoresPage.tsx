import CoresTable from "@/components/CoresTable";
import React from "react";
import { Box, Button, Flex, Text } from "theme-ui";
import useUserSettings from "@/util/userSettings.hook";
import useUserCores from "@/util/userCores.hook";
import useMasterList from "@/util/masterCoresList.hook";
import useFirmware from "@/util/firmware.hook";
import useAppUpdate from "@/util/appUpdate.hook";
import { dataPointStyle, thinRowStyle } from "./CoresPage.style";

const CoresPage: React.FC = () => {
  const { userSettings } = useUserSettings();
  const { userCores } = useUserCores();
  const { masterList } = useMasterList();
  const { myFw, analogueFw } = useFirmware();
  const { updateStatus } = useAppUpdate();

  const openExternal = (url: string): void => {
    require("electron").shell.openExternal(url);
  };

  return (
    <Box sx={{ width: "100%" }}>
      {updateStatus?.updateAvailable && (
        <Box sx={{ p: 2 }}>
          <Flex sx={thinRowStyle}>
            <Box>App update available!</Box>
            <Flex>
              <Text>My version: </Text>
              <Text sx={dataPointStyle}>{updateStatus.myVersion}</Text>
            </Flex>
            <Flex>
              <Text>New version: </Text>
              <Text sx={dataPointStyle}>{updateStatus.currentVersion}</Text>
            </Flex>
            <Button
              variant="fwDl"
              onClick={() =>
                openExternal(
                  "https://gitlab.com/jgdigitaljedi/core-store/-/releases"
                )
              }
            >
              Download
            </Button>
          </Flex>
        </Box>
      )}
      {userSettings?.checkFwUpdates && analogueFw !== "ERROR" && (
        <Box sx={{ p: 2 }}>
          <Flex sx={thinRowStyle}>
            <Box>Analogue Pocket firmware</Box>
            <Flex>
              <Text>Current version: </Text>
              <Text sx={dataPointStyle}>{analogueFw}</Text>
            </Flex>
            <Flex>
              <Text>My version: </Text>
              <Text sx={dataPointStyle}>{myFw}</Text>
            </Flex>
            {myFw !== analogueFw && (
              <Button
                variant="fwDl"
                onClick={() =>
                  require("electron").shell.openExternal(
                    "https://www.analogue.co/support/pocket/firmware"
                  )
                }
                aria-label="Download latest firmware"
              >
                Download
              </Button>
            )}
          </Flex>
        </Box>
      )}
      <CoresTable cores={masterList} userCores={userCores || []} />
    </Box>
  );
};

export default CoresPage;
