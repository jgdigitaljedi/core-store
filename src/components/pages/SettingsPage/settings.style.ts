import { ThemeUIStyleObject } from "theme-ui";

export const checksContainerStyle: ThemeUIStyleObject = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  mb: 3,
};
