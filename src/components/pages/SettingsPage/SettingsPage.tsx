import { ICoreRepo, IUserCore } from "@/util/coreData";
import React from "react";
import { Flex } from "theme-ui";
import { ChecksSection } from "./ChecksSection";
import { CoresVersions } from "./CoresVersions";

const SettingsPage: React.FC = () => {
  return (
    <Flex sx={{ flexDirection: "column" }}>
      <ChecksSection />
      <CoresVersions />
    </Flex>
  );
};

export default SettingsPage;
