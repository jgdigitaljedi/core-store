import { IUserSettings } from "@/util/userSettings";
import useUserSettings from "@/util/userSettings.hook";
import React, { ChangeEvent, ChangeEventHandler } from "react";
import { Flex, Label, Switch } from "theme-ui";
import { useColorMode } from "theme-ui";
import { IoIosSunny } from "react-icons/io";

const ThemeSelector: React.FC = () => {
  const [colorMode, setColorMode] = useColorMode();
  const { userSettings, changeUserSettings } = useUserSettings();

  const onThemeSwitch: ChangeEventHandler = (
    event: ChangeEvent<HTMLInputElement>
  ) => {
    const whichTheme = event.target.checked ? "light" : "default";
    setColorMode(whichTheme);
    const usCopy = { ...userSettings };
    usCopy.theme = whichTheme;
    changeUserSettings(usCopy as IUserSettings);
  };
  return (
    <Flex sx={{ flexDirection: "column" }}>
      <Label htmlFor="theme-selector" variant="settings">
        Theme
      </Label>
      <Flex sx={{ justifyContent: "flex-start", "> label": { width: "auto" } }}>
        <Switch
          onChange={onThemeSwitch}
          checked={userSettings?.theme === "light"}
          id="theme-selector"
        />
        <Label>
          <IoIosSunny size="1.2rem" alt-text="light theme" />
        </Label>
      </Flex>
    </Flex>
  );
};

export default ThemeSelector;
