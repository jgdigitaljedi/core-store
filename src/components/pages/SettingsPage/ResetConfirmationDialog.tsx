import theme from "@/theme";
import React from "react";
import { IoIosRefresh, IoMdClose } from "react-icons/io";
import { Box, Button, Close, Flex, Heading, Text } from "theme-ui";
import "./confirmAlert.css";

interface ResetConfirmationDialogProps {
  onClose: () => void;
  onReset: (closeFn: () => void) => void;
}

export const ResetConfirmationDialog: React.FC<
  ResetConfirmationDialogProps
> = ({ onClose, onReset }) => {
  return (
    <Box
      sx={{
        maxWidth: "30rem",
        backgroundColor: theme.colors?.primary,
        height: "100%",
      }}
    >
      <Flex
        sx={{
          justifyContent: "space-between",
          width: "100%",
          alignItems: "center",
        }}
      >
        <Box> </Box>
        <Heading as="h2" sx={{ fontStyle: "bold", color: theme.colors?.black }}>
          Are you sure?
        </Heading>
        <Close sx={{ color: theme.colors?.black }} onClick={() => onClose()} />
      </Flex>
      <Box sx={{ p: 3 }}>
        <Text sx={{ color: theme.colors?.black }}>
          Are you sure you want to reset all settings? This will not only reset
          settings to default but your cores data and firmware version reset as
          well!
        </Text>
        <Flex
          sx={{
            justifyContent: "flex-end",
            width: "100%",
            borderTopWidth: "1px",
            borderTopColor: theme.colors?.darkLess,
            borderTopStyle: "solid",
            pt: 3,
          }}
        >
          <Button
            sx={{
              mr: 3,
              backgroundColor: theme.colors?.error,
              cursor: "pointer",
              display: "flex",
              alignItems: "center",
            }}
            type="button"
            onClick={() => onReset(onClose)}
          >
            {/**@ts-ignore */}
            <IoIosRefresh size="1.1rem" style={{ "margin-right": ".5rem" }} />
            Reset
          </Button>
          <Button
            onClick={onClose}
            sx={{
              backgroundColor: theme.colors?.bgLighter,
              cursor: "pointer",
              display: "flex",
              alignItems: "center",
            }}
            type="button"
          >
            {/**@ts-ignore */}
            <IoMdClose size="1.1rem" style={{ "margin-right": ".5rem" }} />
            Cancel
          </Button>
        </Flex>
      </Box>
    </Box>
  );
};
