import { ICoreRepo, IUserCore } from "@/util/coreData";
import React, { ChangeEvent, useCallback, useEffect, useState } from "react";
import {
  Box,
  Button,
  Card,
  Field,
  Flex,
  Grid,
  Heading,
  Label,
  Text,
} from "theme-ui";
import debounce from "lodash/debounce";
import truncate from "lodash/truncate";
import useUserCores from "@/util/userCores.hook";
import useMasterList from "@/util/masterCoresList.hook";

interface IUcObj {
  [key: string]: string | null;
}

export const CoresVersions: React.FC = () => {
  const [ucObj, setUcObj] = useState<IUcObj>({});
  const { userCores, changeUserCores } = useUserCores();
  const { masterList } = useMasterList();

  const writeCores = useCallback(
    debounce((ucCopy: IUcObj) => {
      if (ucCopy) {
        // @ts-ignore
        const coresFormatted: IUserCore[] = Object.keys(ucCopy)
          .map((coreId) => {
            return { id: coreId, myVersion: ucCopy[coreId] };
          })
          .filter((core) => core?.myVersion?.length);
        changeUserCores(coresFormatted);
      }
    }, 500),
    [userCores]
  );

  const onFieldChange = (id: string, value: string) => {
    const ucCopy = { ...ucObj };
    ucCopy[id] = value;
    setUcObj(ucCopy);
    writeCores(ucCopy);
  };

  useEffect(() => {
    if (userCores?.length) {
      const uc = userCores.reduce((acc: IUcObj, obj: IUserCore) => {
        acc[obj.id] = obj.myVersion;
        return acc;
      }, {});
      setUcObj(uc);
    }
  }, [userCores]);

  if (!masterList?.length) {
    return <></>;
  }

  return (
    <Card
      sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <Heading as="h2">My cores versions</Heading>
      <Text sx={{ mb: 3, mt: 1 }}>
        Add the versions of each core you are using. Note that by leaving a core
        field blank, the app will not check for updates for that core. The
        buttons beside each core field will automatically fill in that field
        with the latest version of that core. If the core version is truncated
        because it is too long to fit on the button, simply hover over the
        button for a second to see the full version.
      </Text>
      <Grid columns={[1, 2, 2, 3, 4]}>
        {masterList?.length &&
          masterList.map((item: ICoreRepo) => {
            return (
              <Box key={item.id}>
                <Label
                  htmlFor={item.id}
                  sx={{ mb: -2, color: "textLghter", fontWeight: "bold" }}
                >
                  {item.platform}
                </Label>
                <Flex>
                  <Field
                    name={item.id}
                    label={`${item.developer}`}
                    key={item.id}
                    onChange={(event: ChangeEvent<HTMLInputElement>) =>
                      onFieldChange(item.id, event.target.value)
                    }
                    value={ucObj[item.id] || ""}
                    id={item.id}
                  />
                  <Button
                    title={item.release}
                    onClick={() => onFieldChange(item.id, item.release)}
                    type="button"
                  >
                    {truncate(item.release, { length: 8 })}
                  </Button>
                </Flex>
              </Box>
            );
          })}
      </Grid>
    </Card>
  );
};
