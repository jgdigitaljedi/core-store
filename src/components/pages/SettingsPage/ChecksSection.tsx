import useFirmware from "@/util/firmware.hook";
import { useNotifications } from "@/util/notificationContext";
import useUserCores from "@/util/userCores.hook";
import { BoolSettings, IUserSettings } from "@/util/userSettings";
import useUserSettings from "@/util/userSettings.hook";
import React, { ChangeEvent, useEffect, useState } from "react";
import { IoMdWarning } from "react-icons/io";
import { IoSave } from "react-icons/io5";
import { Box, Button, Card, Field, Flex, Label, Switch, Text } from "theme-ui";
import { checksContainerStyle } from "./settings.style";
import ThemeSelector from "./ThemeSelector";
import { confirmAlert } from "react-confirm-alert";
import { ResetConfirmationDialog } from "./ResetConfirmationDialog";

export const ChecksSection: React.FC = () => {
  const { setNotification } = useNotifications();
  const [myFware, setMyFware] = useState<string>("");
  const { userSettings, changeUserSettings } = useUserSettings();
  const { myFw, changeMyFw, fwError } = useFirmware();
  const { changeUserCores } = useUserCores();

  const onToggle = (prop: keyof BoolSettings) => {
    if (userSettings) {
      const settingsCopy: IUserSettings = { ...userSettings };
      settingsCopy[prop] = !userSettings[prop];
      changeUserSettings(settingsCopy);
    }
  };

  const saveMyFw = () => {
    changeMyFw(myFware);
  };

  const resetSettingsAction = (closeAction: () => void) => {
    closeAction();
    const defaultSettings: IUserSettings = {
      theme: "default",
      checkFwUpdates: true,
    };
    changeUserSettings(defaultSettings);
    changeUserCores([]);
    changeMyFw("");
  };

  const resetSettingsDialog = () => {
    // TODO: write this
    confirmAlert({
      closeOnEscape: true,
      closeOnClickOutside: true,
      keyCodeForClose: [8, 32],
      customUI: ({ onClose }) => (
        <ResetConfirmationDialog
          onClose={onClose}
          onReset={resetSettingsAction}
        />
      ),
    });
  };

  useEffect(() => {
    setMyFware(myFw);
    if (fwError?.error) {
      setNotification({ variant: "error", message: fwError.message });
    } else if (fwError && !fwError.error) {
      setNotification({
        variant: "success",
        message: "Your firmware version was updated!",
        timeout: 4000,
      });
    }
  }, [fwError, myFw]);

  if (userSettings) {
    return (
      <Card
        sx={{
          display: "flex",
          width: "100%",
          justifyContent: "space-around",
          flexDirection: ["column", "row"],
        }}
        variant="primary"
      >
        <Flex
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "auto",
          }}
        >
          <ThemeSelector />
          <Box sx={checksContainerStyle}>
            <Label htmlFor="fwUpdates" variant="settings">
              Check firmware updates?
            </Label>
            <Switch
              id="fwUpdates"
              onChange={() => onToggle("checkFwUpdates")}
              checked={userSettings.checkFwUpdates}
            />
          </Box>
        </Flex>
        <Flex
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "auto",
          }}
        >
          <Box sx={{ mb: 3 }}>
            <Button
              onClick={() => resetSettingsDialog()}
              variant="primary"
              sx={{ display: "flex", alignItems: "center" }}
              type="button"
            >
              <IoMdWarning size={"1rem"} alt-text="warning" />
              <Text sx={{ ml: 2 }}>Reset settings and cores</Text>
            </Button>
          </Box>
          <Flex
            sx={{
              my: [4, 4, 0],
              alignItems: "flex-end",
            }}
          >
            <Field
              label="My firmware version"
              value={myFware}
              onChange={(event: ChangeEvent<HTMLInputElement>) =>
                setMyFware(event.target.value)
              }
            />
            <Button
              type="button"
              onClick={() => saveMyFw()}
              sx={{ maxHeight: "4rem" }}
            >
              <IoSave alt-text="save" />
            </Button>
          </Flex>
        </Flex>
      </Card>
    );
  } else {
    return <></>;
  }
};
