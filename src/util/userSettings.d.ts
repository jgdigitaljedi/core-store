export type ThemeOptions = "default" | "light";

export interface BoolSettings {
  checkFwUpdates: boolean;
}

export interface IUserSettings extends BoolSettings {
  theme: ThemeOptions;
}
