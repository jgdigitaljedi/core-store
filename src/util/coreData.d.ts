export interface ICoreRepo {
  id: string;
  release: string;
  publishDate: string;
  url: string;
  description: string;
  download: string;
  platform: string;
  prerelease: boolean;
  repoUrl: string;
  developer: string;
  repo: string;
}

export interface IUserCore {
  id: string;
  myVersion: string;
}

export interface ICombinedCore extends ICoreRepo, IUserCore {}
