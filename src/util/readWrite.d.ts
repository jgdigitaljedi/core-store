export interface ICrudError {
  error: boolean;
  message: string;
  code?: any;
  ver?: string;
}
