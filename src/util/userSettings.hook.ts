import { useCallback, useEffect, useState } from "react";
import channels from "../../shared/channels";
import { IUserSettings } from "./userSettings";
const { ipcRenderer } = window.require("electron");

interface SettingsHook {
  userSettings: IUserSettings | null;
  changeUserSettings: (userSettings: IUserSettings) => void;
}

const useUserSettings = (): SettingsHook => {
  const [userSettings, setUserSettings] = useState<IUserSettings | null>(null);

  const listenForChanges = useCallback(() => {
    ipcRenderer.on(
      channels.USER_SETTINGS,
      (event: Event, settings: IUserSettings) => {
        setUserSettings(settings);
      }
    );
  }, []);

  useEffect(() => {
    ipcRenderer.send(channels.USER_SETTINGS);
    listenForChanges();

    return () => {
      ipcRenderer.removeAllListeners(channels.USER_SETTINGS);
    };
  }, []);

  const changeUserSettings = (newSettings: IUserSettings) => {
    ipcRenderer.send(channels.SET_USER_SETTINGS, newSettings);
    listenForChanges();
  };

  return { userSettings, changeUserSettings };
};

export default useUserSettings;
