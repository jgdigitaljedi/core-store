import { useCallback, useEffect, useState } from "react";
import channels from "../../shared/channels";
import { IUserCore } from "./coreData";
const { ipcRenderer } = window.require("electron");

interface UcHook {
  userCores: IUserCore[] | null;
  changeUserCores: (userCores: IUserCore[]) => void;
}

const useUserCores = (): UcHook => {
  const [userCores, setUserCores] = useState<IUserCore[] | null>(null);

  const listenForChanges = useCallback(() => {
    ipcRenderer.on(channels.USER_CORES, (event: Event, cores: IUserCore[]) => {
      setUserCores(cores);
    });
  }, []);

  useEffect(() => {
    ipcRenderer.send(channels.USER_CORES);
    listenForChanges();

    return () => {
      ipcRenderer.removeAllListeners(channels.USER_CORES);
    };
  }, []);

  const changeUserCores = (newCores: IUserCore[]) => {
    ipcRenderer.send(channels.SET_USER_CORES, newCores);
    listenForChanges();
  };

  return { userCores, changeUserCores };
};

export default useUserCores;
