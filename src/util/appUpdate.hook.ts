import { useCallback, useEffect, useState } from "react";
import channels from "../../shared/channels";
const { ipcRenderer } = window.require("electron");
import packageJson from "../../package.json";
import { ICrudError } from "./readWrite";

export interface UpdateStatus {
  currentVersion: string | null;
  myVersion: string;
  updateAvailable: boolean;
  updateError: ICrudError | null;
}

interface AppUpdateHook {
  updateStatus: any;
}

const useAppUpdate = (): AppUpdateHook => {
  const [updateStatus, setUpdateStatus] = useState<UpdateStatus | null>(null);

  const compareVersions = (currentVersion: object | ICrudError) => {
    const cv =
      // @ts-ignore
      typeof currentVersion?.version === "string"
        ? // @ts-ignore
          currentVersion.version
        : null;
    setUpdateStatus({
      myVersion: packageJson.version,
      currentVersion: cv,
      updateAvailable: cv ? cv !== packageJson.version : false,
      updateError: !cv ? (currentVersion as ICrudError) : null,
    });
  };

  const listenForChanges = useCallback(() => {
    ipcRenderer.on(
      channels.APP_UPDATE,
      (event: Event, currentVersion: object | ICrudError) => {
        compareVersions(currentVersion);
      }
    );
  }, []);

  useEffect(() => {
    ipcRenderer.send(channels.APP_UPDATE);
    listenForChanges();

    return () => {
      ipcRenderer.removeAllListeners(channels.APP_UPDATE);
    };
  }, []);

  return { updateStatus };
};

export default useAppUpdate;
