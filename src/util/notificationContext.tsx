import Notify from "@/components/Notify";
import * as React from "react";

export type NOTIFICATION = "primary" | "error" | "success" | "warning" | "info";

export interface AddNotification {
  message: string | React.ReactNode;
  timeout?: number;
  variant?: NOTIFICATION;
}

interface Notification {
  id: number;
  message: string | React.ReactNode;
  variant: NOTIFICATION;
  timeout?: number;
}

const defaultApi = {
  notifications: [] as Notification[],
  setNotification: (notification: AddNotification) => null,
  clearNotification: (id: number) => null,
};

export type NotificationsContextValue = typeof defaultApi;

/**
 * Create Context
 */
export const NotificationsContext =
  React.createContext<NotificationsContextValue>(defaultApi);

/**
 * Custom Notifications Provider
 */
export function NotificationsProvider({ children }: any) {
  // Notifications queue is managed in local useState
  const [notifications, setNotifications] = React.useState<Notification[]>(
    defaultApi.notifications
  );

  // Method to push a new notification
  const setNotification = React.useCallback(
    (notification: AddNotification) => {
      const nextNotifications = notifications.concat({
        id: new Date().getTime(),
        variant: notification.variant || "primary",
        timeout: notification.timeout,
        ...notification,
      } as Notification);
      setNotifications(nextNotifications);
    },
    [notifications, setNotifications]
  );

  // Method to clear a notification
  const clearNotification = React.useCallback(
    (id: number) => {
      const nextNotifications = notifications.filter((n) => n.id !== id);
      setNotifications(nextNotifications);
    },
    [notifications, setNotifications]
  );

  // Return Provider with full API
  const api = { notifications, setNotification, clearNotification };
  return (
    // @ts-ignore
    <NotificationsContext.Provider value={api}>
      <Notify />
      {children}
    </NotificationsContext.Provider>
  );
}

export function useNotifications() {
  return React.useContext(NotificationsContext);
}
