import { useCallback, useEffect, useState } from "react";
import channels from "../../shared/channels";
import { ICrudError } from "./readWrite";
const { ipcRenderer } = window.require("electron");

interface FirmwareHook {
  myFw: string;
  analogueFw: string;
  changeMyFw: (fw: string) => void;
  fwError: ICrudError | null;
}

const useFirmware = (): FirmwareHook => {
  const [myFw, setMyFw] = useState<string>("");
  const [analogueFw, setAnalogueFw] = useState<string>("");
  const [fwError, setFwError] = useState<ICrudError | null>(null);

  const listenForChanges = useCallback(() => {
    ipcRenderer.on(channels.MY_FW, (event: Event, fw: string | ICrudError) => {
      if (typeof fw === "string") {
        setMyFw(fw);
      } else if (typeof fw === "object" && !fw.error && !!fw.ver) {
        setMyFw(fw.ver);
        setFwError(fw);
      } else {
        setFwError(fw);
      }
    });
  }, []);

  useEffect(() => {
    ipcRenderer.send(channels.ANALOGUE_FW);
    ipcRenderer.send(channels.MY_FW);
    listenForChanges();
    ipcRenderer.on(
      channels.ANALOGUE_FW,
      (event: Event, fw: string | ICrudError) => {
        if (typeof fw === "string") {
          setAnalogueFw(fw);
        } else {
          setFwError(fw);
        }
      }
    );

    return () => {
      ipcRenderer.removeAllListeners(channels.ANALOGUE_FW, channels.MY_FW);
    };
  }, []);

  const changeMyFw = (fw: string) => {
    ipcRenderer.send(channels.SET_MY_FW, fw);
    console.log("setMyFW", fw);
  };

  return { myFw, analogueFw, changeMyFw, fwError };
};

export default useFirmware;
