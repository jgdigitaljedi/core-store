import { useCallback, useEffect, useState } from "react";
import channels from "../../shared/channels";
import { ICoreRepo } from "./coreData";
const { ipcRenderer } = window.require("electron");

interface MasterListHook {
  masterList: ICoreRepo[] | null;
}

const useMasterList = (): MasterListHook => {
  const [masterList, setMasterList] = useState<ICoreRepo[] | null>(null);

  const listenForChanges = useCallback(() => {
    ipcRenderer.on(
      channels.MASTER_REPO_LIST,
      (event: Event, cores: ICoreRepo[]) => {
        setMasterList(cores);
      }
    );
  }, []);

  useEffect(() => {
    ipcRenderer.send(channels.MASTER_REPO_LIST);
    listenForChanges();

    return () => {
      ipcRenderer.removeAllListeners(channels.MASTER_REPO_LIST);
    };
  }, []);

  return { masterList };
};

export default useMasterList;
