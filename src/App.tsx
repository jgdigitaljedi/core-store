import React, { useEffect, useState } from "react";
import { Box, ThemeProvider } from "theme-ui";
import theme from "./theme";
import Navbar from "./components/Navbar";
import { IUserCore } from "./util/coreData";
import { Route, Routes } from "react-router-dom";
import CoresPage from "./components/pages/CoresPage";
import SettingsPage from "./components/pages/SettingsPage";
import AboutPage from "./components/pages/AboutPage";
import { NotificationsProvider } from "./util/notificationContext";
const { ipcRenderer } = window.require("electron");

const App: React.FC = () => {
  useEffect(() => {
    ipcRenderer.send("resize-window", 1280, 720);
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Box sx={{ p: 3 }}>
        <NotificationsProvider>
          <Navbar />
          <Routes>
            <Route path="/settings" element={<SettingsPage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/" element={<CoresPage />} />
          </Routes>
        </NotificationsProvider>
      </Box>
    </ThemeProvider>
  );
};

export default App;
