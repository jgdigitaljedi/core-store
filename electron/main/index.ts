// The built directory structure
//
// ├─┬ dist
// │ ├─┬ electron
// │ │ ├─┬ main
// │ │ │ └── index.js
// │ │ └─┬ preload
// │ │   └── index.js
// │ ├── index.html
// │ ├── ...other-static-files-from-public
// │
process.env.DIST = join(__dirname, "../..");
process.env.PUBLIC = app.isPackaged
  ? process.env.DIST
  : join(process.env.DIST, "../public");
process.env["ELECTRON_DISABLE_SECURITY_WARNINGS"] = "true";

import { app, BrowserWindow, shell, ipcMain } from "electron";
import { release } from "os";
import { join } from "path";
import getMasterRepoList from "../../api/getMasterRepoList";
import checkAppUpdate from "../../api/checkAppUpdate";
import { getMyFw, setMyFw } from "../../api/getMyFw";
import { getPocketFirmwareVersion } from "../../api/getPocketFirmware";
import { getUserCores, setUserCores } from "../../api/userCoresCrud";
import { getSettings, updateSettings } from "../../api/userSettings";
import channels from "../../shared/channels";

// Disable GPU Acceleration for Windows 7
if (release().startsWith("6.1")) app.disableHardwareAcceleration();

// Set application name for Windows 10+ notifications
if (process.platform === "win32") app.setAppUserModelId(app.getName());

if (!app.requestSingleInstanceLock()) {
  app.quit();
  process.exit(0);
}

let win: BrowserWindow | null = null;
// Here, you can also use other preload
const preload = join(__dirname, "../preload/index.js");
const url = process.env.VITE_DEV_SERVER_URL;
const indexHtml = join(process.env.DIST, "index.html");

async function createWindow() {
  win = new BrowserWindow({
    title: "Analogue Pocket Core Store",
    // @ts-ignore
    icon: join(process.env.PUBLIC, "favicon.svg"),
    webPreferences: {
      preload,
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  win.removeMenu(); // TODO: uncomment this before build

  if (app.isPackaged) {
    win.loadFile(indexHtml);
  } else {
    // @ts-ignore
    win.loadURL(url);
    // win.webContents.openDevTools()
  }

  // Test actively push message to the Electron-Renderer
  win.webContents.on("did-finish-load", () => {
    win?.webContents.send("main-process-message", new Date().toLocaleString());
  });

  // Make all links open with the browser, not with the application
  win.webContents.setWindowOpenHandler(({ url }) => {
    if (url.startsWith("https:")) shell.openExternal(url);
    return { action: "deny" };
  });
}

app.whenReady().then(createWindow);

app.on("before-quit", () => {
  win?.webContents.send("app-close");
});

app.on("window-all-closed", () => {
  win = null;
  if (process.platform !== "darwin") app.quit();
});

app.on("second-instance", () => {
  if (win) {
    // Focus on the main window if the user tried to open another
    if (win.isMinimized()) win.restore();
    win.focus();
  }
});

app.on("activate", () => {
  const allWindows = BrowserWindow.getAllWindows();
  if (allWindows.length) {
    allWindows[0].focus();
  } else {
    createWindow();
  }
});

// new window example arg: new windows url
ipcMain.handle("open-win", (event, arg) => {
  const childWindow = new BrowserWindow({
    webPreferences: {
      preload,
    },
  });

  if (app.isPackaged) {
    childWindow.loadFile(indexHtml, { hash: arg });
  } else {
    childWindow.loadURL(`${url}/#${arg}`);
    // childWindow.webContents.openDevTools({ mode: "undocked", activate: true })
  }
});

ipcMain.on("resize-window", (event, width, height) => {
  let browserWindow = BrowserWindow.fromWebContents(event.sender);
  browserWindow.setSize(width, height);
});

ipcMain.on(channels.APP_UPDATE, async (event) => {
  const currentVersion = await checkAppUpdate();
  event.sender.send(channels.APP_UPDATE, currentVersion);
});

ipcMain.on(channels.MASTER_REPO_LIST, async (event) => {
  const list = await getMasterRepoList();
  event.sender.send(channels.MASTER_REPO_LIST, list);
});

ipcMain.on(channels.USER_CORES, (event) => {
  const userCores = getUserCores();
  event.sender.send(channels.USER_CORES, userCores);
});

ipcMain.on(channels.SET_USER_CORES, (event, args) => {
  const userCores = setUserCores(args);
  event.sender.send(channels.USER_CORES, userCores);
});

ipcMain.on(channels.USER_SETTINGS, (event) => {
  const userSettings = getSettings();
  event.sender.send(channels.USER_SETTINGS, userSettings);
});

ipcMain.on(channels.SET_USER_SETTINGS, (event, args) => {
  const userSettings = updateSettings(args);
  event.sender.send(channels.USER_SETTINGS, userSettings);
});

ipcMain.on(channels.ANALOGUE_FW, async (event) => {
  const pocketFw = await getPocketFirmwareVersion();
  event.sender.send(channels.ANALOGUE_FW, pocketFw);
});

ipcMain.on(channels.MY_FW, (event) => {
  const myFw = getMyFw();
  event.sender.send(channels.MY_FW, myFw);
});

ipcMain.on(channels.SET_MY_FW, (event, myFw) => {
  const fwSet = setMyFw(myFw);
  event.sender.send(channels.MY_FW, fwSet);
});
