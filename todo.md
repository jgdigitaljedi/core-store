# TODO

## Settings

## Cores

- (requirement) alerts on load if new cores in app; maybe even add badge to core row
- (fast-follow) skeleton loaders

## About

- (fast-follow) tip, buy coffee, donate, whatever

## General

- style refinement
- add standard version, husky, commitlint so I can wire in update checks; I'll never remeber to increment versions otherwise
- build
- release

## Future

- develop and release where you can plug in SD card, use file navigator to select root of card, and download and extract updates and cores directly to card
  - extraction logic needed from my script version of this
- add GitHub API integration, allow for using personal access token to get updates live from GH instead of relying on me to update my other script with core data every day
- (maybe) throw up a server that perdiodically polls for updates and new cores to solve the "wait for me to update" problem
- (fast-follow) add abiity to scrape sd card for core versions and auto populate them
- (fast-follow) add ability to scrape ROMS and create view with all ROMs that is searchable
